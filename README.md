Bet-victor
==========

Setup and running:
Before run application please provide data source info (config.yml)
------------------
### First time setup ###
    cd <workspace>/bet-victor-task
    $ mvn clean package
### Starting up ###
    cd bet-victor-bundle
    $  java -jar target/task-bundle-0.0.1-SNAPSHOT.jar --spring.config.location=classpath:properties/config.yml
### Endpoint to modify comments ###
    $ http://localhost:8080/bet-victor/swagger
### Dashboard ###
    $ http://localhost:8080/bet-victor/index

Technology Stack
------------------
- Spring Boot
- Spring Data
- Flyway
- Postgresql
- HSQLDB
- DBSetup, Mockito, assertJ
- Spring Web Socket
- Lombok

Because of better performance tests are runnig in hsqldb. During launching app flyway scripts are runned and migrating scripts to db.
All throwed error are handled by ErrorHandler and transform to ResponseEntity. Swagger provide more operation to handle comments like: deleting, getting, updating.

