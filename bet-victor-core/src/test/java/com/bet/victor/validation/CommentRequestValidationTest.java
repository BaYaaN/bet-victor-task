package com.bet.victor.validation;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;

import org.junit.Before;
import org.junit.Test;

import com.bet.victor.domain.CommentRequest;

/**
 * Created by Jakub_Baran on 5/11/2016.
 */
public class CommentRequestValidationTest {

    private final Validator validator = Validation.buildDefaultValidatorFactory().getValidator();
    private CommentRequest commentRequest;

    @Before
    public void init() {
        commentRequest = createProperCommentRequest();
    }

    @Test
    public void shouldValidateWithProperValue() {
        //when
        Set<ConstraintViolation<CommentRequest>> constraintViolations = validator.validate(commentRequest);

        //then
        assertThat(constraintViolations).hasSize(0);
    }

    @Test
    public void shouldNotValidateRequestWithMissingContent() {
        //given
        commentRequest.setContent(null);

        //when
        Set<ConstraintViolation<CommentRequest>> constraintViolations = validator.validate(commentRequest);

        //then
        assertThat(constraintViolations).hasSize(1);
    }

    private CommentRequest createProperCommentRequest() {
        CommentRequest commentRequest = new CommentRequest();
        commentRequest.setContent("Not empty");

        return commentRequest;
    }
}
