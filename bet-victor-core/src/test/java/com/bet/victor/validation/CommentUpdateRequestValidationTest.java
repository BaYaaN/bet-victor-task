package com.bet.victor.validation;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;

import org.junit.Before;
import org.junit.Test;

import com.bet.victor.domain.CommentRequest;
import com.bet.victor.domain.CommentUpdateRequest;

/**
 * Created by Jakub_Baran on 5/11/2016.
 */
public class CommentUpdateRequestValidationTest {

    private final Validator validator = Validation.buildDefaultValidatorFactory().getValidator();
    private CommentUpdateRequest commenUpdatetRequest;

    @Before
    public void init() {
        commenUpdatetRequest = createProperCommentRequest();
    }

    @Test
    public void shouldValidateWithProperValue() {
        //when
        Set<ConstraintViolation<CommentRequest>> constraintViolations = validator.validate(commenUpdatetRequest);

        //then
        assertThat(constraintViolations).hasSize(0);
    }

    @Test
    public void shouldNotValidateRequestWithMissingContent() {
        //given
        commenUpdatetRequest.setContent(null);

        //when
        Set<ConstraintViolation<CommentRequest>> constraintViolations = validator.validate(commenUpdatetRequest);

        //then
        assertThat(constraintViolations).hasSize(1);
    }

    @Test
    public void shouldNotValidateRequestWithMissingId() {
        //given
        commenUpdatetRequest.setId(null);

        //when
        Set<ConstraintViolation<CommentRequest>> constraintViolations = validator.validate(commenUpdatetRequest);

        //then
        assertThat(constraintViolations).hasSize(1);
    }

    @Test
    public void shouldNotValidateRequestWithMissingContentAndId() {
        //given
        commenUpdatetRequest.setContent(null);
        commenUpdatetRequest.setId(null);

        //when
        Set<ConstraintViolation<CommentRequest>> constraintViolations = validator.validate(commenUpdatetRequest);

        //then
        assertThat(constraintViolations).hasSize(2);
    }

    private CommentUpdateRequest createProperCommentRequest() {
        CommentUpdateRequest commentUpdateRequest = new CommentUpdateRequest();
        commentUpdateRequest.setContent("Not empty");
        commentUpdateRequest.setId(1L);

        return commentUpdateRequest;
    }
}
