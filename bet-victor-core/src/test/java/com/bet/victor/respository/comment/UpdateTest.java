package com.bet.victor.respository.comment;

import com.bet.victor.common.AbstractRepositoryTest;
import com.bet.victor.domain.Status;
import com.bet.victor.entity.CommentEntity;
import com.bet.victor.repository.CommentRepository;
import com.ninja_squad.dbsetup.DbSetup;
import com.ninja_squad.dbsetup.destination.DataSourceDestination;
import com.ninja_squad.dbsetup.operation.Operation;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static com.ninja_squad.dbsetup.operation.CompositeOperation.sequenceOf;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

/**
 * Created by Kuba on 2016-05-06.
 */
public class UpdateTest extends AbstractRepositoryTest {

    @Autowired
    private CommentRepository commentRepository;

    @Before
    public void prepare() {
        Operation operation = sequenceOf(DELETE_ALL, INSERT_INTO_COMMENT);
        DbSetup dbSetup = new DbSetup(new DataSourceDestination(dataSource), operation);
        dbSetup.launch();
    }

    @Test
    public void shouldUpdateOneItem() {
        //given
        CommentEntity itemToUpdate = commentRepository.findOne(1L);

        //when
        itemToUpdate.setStatus(Status.REJECTED);
        itemToUpdate.setContent("updated");
        commentRepository.save(itemToUpdate);

        //then
        CommentEntity updatedItem = commentRepository.findOne(1L);
        assertThat(updatedItem).isNotNull();
        assertThat(updatedItem.getId()).isEqualTo(1L);
        assertThat(updatedItem.getStatus()).isEqualTo(Status.REJECTED);
        assertThat(updatedItem.getContent()).isEqualTo("updated");
    }
}
