package com.bet.victor.respository.comment;

import com.bet.victor.common.AbstractRepositoryTest;
import com.bet.victor.domain.Status;
import com.bet.victor.entity.CommentEntity;
import com.bet.victor.repository.CommentRepository;
import com.ninja_squad.dbsetup.DbSetup;
import com.ninja_squad.dbsetup.destination.DataSourceDestination;
import com.ninja_squad.dbsetup.operation.Operation;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Iterator;
import java.util.List;

import static com.ninja_squad.dbsetup.operation.CompositeOperation.sequenceOf;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

/**
 * Created by Kuba on 2016-05-08.
 */
public class FindByStatusTest extends AbstractRepositoryTest {

    @Autowired
    private CommentRepository commentRepository;

    @Before
    public void prepare() {
        Operation operation = sequenceOf(DELETE_ALL, INSERT_INTO_COMMENT);
        DbSetup dbSetup = new DbSetup(new DataSourceDestination(dataSource), operation);
        dbSetup.launch();
    }

    @Test
    public void shouldFindAllItemsWithAcceptedStatus() {

        //when
        List<CommentEntity> comments = commentRepository.findByStatus(Status.ACCEPTED);

        //then
        assertThat(comments.size()).isEqualTo(3);

        Iterator<CommentEntity> iterator = comments.iterator();
        CommentEntity commentEntity = iterator.next();
        assertThat(commentEntity.getId()).isEqualTo(1);
        assertThat(commentEntity.getStatus()).isEqualTo(Status.ACCEPTED);

        commentEntity = iterator.next();
        assertThat(commentEntity.getId()).isEqualTo(2);
        assertThat(commentEntity.getStatus()).isEqualTo(Status.ACCEPTED);

        commentEntity = iterator.next();
        assertThat(commentEntity.getId()).isEqualTo(3);
        assertThat(commentEntity.getStatus()).isEqualTo(Status.ACCEPTED);

    }

    @Test
    public void shouldFindAllItemsWithRejectedStatus() {

        //when
        List<CommentEntity> comments = commentRepository.findByStatus(Status.REJECTED);

        //then
        assertThat(comments.size()).isEqualTo(2);

        Iterator<CommentEntity> iterator = comments.iterator();
        CommentEntity commentEntity = iterator.next();
        assertThat(commentEntity.getId()).isEqualTo(4);
        assertThat(commentEntity.getStatus()).isEqualTo(Status.REJECTED);

        commentEntity = iterator.next();
        assertThat(commentEntity.getId()).isEqualTo(5);
        assertThat(commentEntity.getStatus()).isEqualTo(Status.REJECTED);
    }

    @Test
    public void shouldReturnEmptyListWithToVerifyStatus() {

        //when
        List<CommentEntity> comments = commentRepository.findByStatus(Status.TO_VERIFY);

        //then
        assertThat(comments.size()).isEqualTo(0);
    }
}
