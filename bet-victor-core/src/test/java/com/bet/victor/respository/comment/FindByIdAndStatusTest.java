package com.bet.victor.respository.comment;

import com.bet.victor.common.AbstractRepositoryTest;
import com.bet.victor.domain.Status;
import com.bet.victor.entity.CommentEntity;
import com.bet.victor.repository.CommentRepository;
import com.ninja_squad.dbsetup.DbSetup;
import com.ninja_squad.dbsetup.destination.DataSourceDestination;
import com.ninja_squad.dbsetup.operation.Operation;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static com.ninja_squad.dbsetup.operation.CompositeOperation.sequenceOf;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

/**
 * Created by Kuba on 2016-05-08.
 */
public class FindByIdAndStatusTest extends AbstractRepositoryTest {
    @Autowired
    private CommentRepository commentRepository;

    @Before
    public void prepare() {
        Operation operation = sequenceOf(DELETE_ALL, INSERT_INTO_COMMENT);
        DbSetup dbSetup = new DbSetup(new DataSourceDestination(dataSource), operation);
        dbSetup.launch();
    }

    @Test
    public void shouldFindByIdAndStatusAccepted() {

        //when
        CommentEntity comment = commentRepository.findByIdAndStatus(1, Status.ACCEPTED);

        //then
        assertThat(comment).isNotNull();
        assertThat(comment.getId()).isEqualTo(1);
        assertThat(comment.getStatus()).isEqualTo(Status.ACCEPTED);
    }

    @Test
    public void shouldFindByIdAndStatusRejected() {

        //when
        CommentEntity comment = commentRepository.findByIdAndStatus(4, Status.REJECTED);

        //then
        assertThat(comment).isNotNull();
        assertThat(comment.getId()).isEqualTo(4);
        assertThat(comment.getStatus()).isEqualTo(Status.REJECTED);
    }

    @Test
    public void shouldReturnWhenStatusRejectedButWrongId() {

        //when
        CommentEntity comment = commentRepository.findByIdAndStatus(6, Status.REJECTED);

        //then
        assertThat(comment).isNull();
    }

    @Test
    public void shouldReturnWhenWrongStatus() {

        //when
        CommentEntity comment = commentRepository.findByIdAndStatus(6, Status.TO_VERIFY);

        //then
        assertThat(comment).isNull();
    }
}
