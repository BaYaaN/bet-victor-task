package com.bet.victor.respository.comment;

import com.bet.victor.common.AbstractRepositoryTest;
import com.bet.victor.domain.Status;
import com.bet.victor.entity.CommentEntity;
import com.bet.victor.repository.CommentRepository;
import com.ninja_squad.dbsetup.DbSetup;
import com.ninja_squad.dbsetup.destination.DataSourceDestination;
import com.ninja_squad.dbsetup.operation.Operation;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static com.ninja_squad.dbsetup.operation.CompositeOperation.sequenceOf;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

/**
 * Created by Kuba on 2016-05-06.
 */
public class SaveTest extends AbstractRepositoryTest {

    @Autowired
    private CommentRepository commentRepository;

    @Before
    public void prepare() {
        Operation operation = sequenceOf(DELETE_ALL);
        DbSetup dbSetup = new DbSetup(new DataSourceDestination(dataSource), operation);
        dbSetup.launch();
    }

    @Test
    public void shouldSaveOneItem() {
        //given
        CommentEntity itemToSave = new CommentEntity();
        itemToSave.setContent("Content for test purpose");
        itemToSave.setStatus(Status.ACCEPTED);

        //when
        commentRepository.save(itemToSave);

        //then
        CommentEntity savedItem = commentRepository.findOne(1L);
        assertThat(savedItem).isNotNull();
        assertThat(savedItem.getId()).isEqualTo(1L);
        assertThat(savedItem.getContent()).isEqualTo("Content for test purpose");
        assertThat(savedItem.getStatus()).isEqualTo(Status.ACCEPTED);
    }
}
