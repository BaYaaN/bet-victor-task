package com.bet.victor.respository.comment;

import com.bet.victor.common.AbstractRepositoryTest;
import com.bet.victor.entity.CommentEntity;
import com.bet.victor.repository.CommentRepository;
import com.ninja_squad.dbsetup.DbSetup;
import com.ninja_squad.dbsetup.destination.DataSourceDestination;
import com.ninja_squad.dbsetup.operation.Operation;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static com.ninja_squad.dbsetup.operation.CompositeOperation.sequenceOf;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

/**
 * Created by Kuba on 2016-05-06.
 */
public class DeleteTest extends AbstractRepositoryTest {

    @Autowired
    private CommentRepository commentRepository;

    @Before
    public void prepare() {
        Operation operation = sequenceOf(DELETE_ALL, INSERT_INTO_COMMENT);
        DbSetup dbSetup = new DbSetup(new DataSourceDestination(dataSource), operation);
        dbSetup.launch();
    }

    @Test
    public void shouldDeleteOneItem() {
        //given
        CommentEntity itemToDelete = commentRepository.findOne(1L);

        //when
        commentRepository.delete(itemToDelete);

        //then
        assertThat(commentRepository.count()).isEqualTo(ROW_COUNT_AFTER_DELETE_ONE_ITEM);
        assertThat(commentRepository.findOne(1L)).isNull();
    }

    @Test
    public void shouldDeleteAllItems() {
        //when
        commentRepository.deleteAll();

        //then
        assertThat(commentRepository.count()).isEqualTo(NONE_ELEMET);
    }
}
