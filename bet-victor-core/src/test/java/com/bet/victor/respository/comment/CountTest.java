package com.bet.victor.respository.comment;

import com.bet.victor.common.AbstractRepositoryTest;
import com.bet.victor.repository.CommentRepository;
import com.ninja_squad.dbsetup.DbSetup;
import com.ninja_squad.dbsetup.destination.DataSourceDestination;
import com.ninja_squad.dbsetup.operation.Operation;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static com.ninja_squad.dbsetup.operation.CompositeOperation.sequenceOf;
import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;

/**
 * Created by Jakub_Baran on 5/6/2016.
 */
public class CountTest extends AbstractRepositoryTest {

    @Autowired
    private CommentRepository commentRepository;

    @Before
    public void prepare() {
        Operation operation = sequenceOf(DELETE_ALL, INSERT_INTO_COMMENT);
        DbSetup dbSetup = new DbSetup(new DataSourceDestination(dataSource), operation);
        dbSetup.launch();
    }

    @Test
    public void shouldFindProperRowCount() {
        assertThat(commentRepository.count()).isEqualTo(ROW_COUNT);
    }
}
