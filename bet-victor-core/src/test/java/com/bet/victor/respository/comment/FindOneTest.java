package com.bet.victor.respository.comment;

import com.bet.victor.common.AbstractRepositoryTest;
import com.bet.victor.entity.CommentEntity;
import com.bet.victor.repository.CommentRepository;
import com.ninja_squad.dbsetup.DbSetup;
import com.ninja_squad.dbsetup.destination.DataSourceDestination;
import com.ninja_squad.dbsetup.operation.Operation;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static com.ninja_squad.dbsetup.operation.CompositeOperation.sequenceOf;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

/**
 * Created by Kuba on 2016-05-08.
 */
public class FindOneTest extends AbstractRepositoryTest {
    @Autowired
    private CommentRepository commentRepository;

    @Before
    public void prepare() {
        Operation operation = sequenceOf(DELETE_ALL, INSERT_INTO_COMMENT);
        DbSetup dbSetup = new DbSetup(new DataSourceDestination(dataSource), operation);
        dbSetup.launch();
    }

    @Test
    public void shouldFindOneItem() {

        //when
        CommentEntity comment = commentRepository.findOne(1L);

        //then
        assertThat(comment).isNotNull();
        assertThat(comment.getId()).isEqualTo(1L);
    }

    @Test
    public void shouldReturnNull() {

        //when
        CommentEntity comment = commentRepository.findOne(10L);

        //then
        assertThat(comment).isNull();
    }
}
