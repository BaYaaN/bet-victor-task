package com.bet.victor.domain;

import com.bet.victor.common.BaseModelTest;
import org.junit.Before;

/**
 * Created by Kuba on 2016-05-08.
 */
public class CommentRequestTest extends BaseModelTest<CommentRequest> {
    @Before
    public void setUp() {
        clazz = CommentRequest.class;
    }
}
