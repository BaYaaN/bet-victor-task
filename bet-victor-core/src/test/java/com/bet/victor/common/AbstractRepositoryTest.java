package com.bet.victor.common;

import com.ninja_squad.dbsetup.operation.Operation;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.WebIntegrationTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.sql.DataSource;

import static com.ninja_squad.dbsetup.Operations.deleteAllFrom;
import static com.ninja_squad.dbsetup.Operations.insertInto;

/**
 * Created by Jakub_Baran on 5/6/2016.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = TestAppConfiguration.class)
@WebIntegrationTest("{server:, port: ${server.port}}")
public abstract class AbstractRepositoryTest {

    @Autowired
    protected DataSource dataSource;

    protected static final int ROW_COUNT = 5;
    protected static final int ROW_COUNT_AFTER_DELETE_ONE_ITEM = 4;
    protected static final int NONE_ELEMET = 0;
    protected final Operation DELETE_ALL = deleteAllFrom("comment");
    protected final Operation INSERT_INTO_COMMENT = insertInto("comment")
            .columns("id", "content", "status")
            .values(1L, "test1", "ACCEPTED")
            .values(2L, "test2", "ACCEPTED")
            .values(3L, "test3", "ACCEPTED")
            .values(4L, "test4", "REJECTED")
            .values(5L, "test5", "REJECTED")
            .build();
}
