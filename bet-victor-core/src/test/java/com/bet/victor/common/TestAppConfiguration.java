package com.bet.victor.common;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.orm.jpa.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

/**
 * Created by Jakub_Baran on 5/6/2016.
 */
@EnableAutoConfiguration
@EntityScan(basePackages = "com.bet.victor.entity")
@EnableJpaRepositories(basePackages = "com.bet.victor.repository")
@ComponentScan({ "com.bet.victor" })
public class TestAppConfiguration {
}
