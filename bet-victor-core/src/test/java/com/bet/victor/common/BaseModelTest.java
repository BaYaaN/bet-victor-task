package com.bet.victor.common;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.dropwizard.jackson.Jackson;
import org.junit.Test;

import java.io.IOException;

import static io.dropwizard.testing.FixtureHelpers.fixture;
import static org.hamcrest.MatcherAssert.assertThat;
import static uk.co.datumedge.hamcrest.json.SameJSONAs.sameJSONAs;

/**
 * Created by Kuba on 2016-05-08.
 */
public abstract class BaseModelTest<T> {

    protected final ObjectMapper MAPPER = Jackson.newObjectMapper();

    protected final String SUFFIX = "Schema.json";

    protected Class<T> clazz;

    @Test
    public void serializeToJSON() throws IOException, IllegalAccessException, InstantiationException {
        assertThat(MAPPER.writeValueAsString(clazz.newInstance()), sameJSONAs(fixture(clazz.getSimpleName() + SUFFIX)));
    }
}
