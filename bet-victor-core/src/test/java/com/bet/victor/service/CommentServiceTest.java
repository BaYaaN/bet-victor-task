package com.bet.victor.service;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.when;

import java.net.URISyntaxException;
import java.util.NoSuchElementException;

import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.bet.victor.domain.CommentUpdateRequest;
import com.bet.victor.domain.Status;
import com.bet.victor.repository.CommentRepository;

/**
 * Created by Jakub_Baran on 5/11/2016.
 */
@RunWith(MockitoJUnitRunner.class)
public class CommentServiceTest {

    @InjectMocks
    private CommentService commentService;

    @Mock
    private CommentRepository commentRepository;

    @Test
    public void shouldThrwoExceptionWhenNull() throws URISyntaxException {
        //given
        when(commentRepository.findByIdAndStatus(anyLong(), any(Status.class))).thenReturn(null);

        // then
        Assertions.assertThatThrownBy(() -> {
            commentService.findByIdAndAcceptedStatus(1L);
        }).isInstanceOf(NoSuchElementException.class);
    }

    @Test
    public void shouldThrwoNoSuchElementWhenNull() throws URISyntaxException {
        //given
        when(commentRepository.findOne(anyLong())).thenReturn(null);

        // then
        Assertions.assertThatThrownBy(() -> {
            commentService.update(new CommentUpdateRequest());
        }).isInstanceOf(NoSuchElementException.class);
    }
}
