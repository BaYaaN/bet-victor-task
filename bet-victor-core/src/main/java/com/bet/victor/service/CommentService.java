package com.bet.victor.service;

import com.bet.victor.domain.CommentRequest;
import com.bet.victor.domain.CommentUpdateRequest;
import com.bet.victor.domain.Status;
import com.bet.victor.entity.CommentEntity;
import com.bet.victor.repository.CommentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.NoSuchElementException;

/**
 * Created by Kuba on 2016-05-08.
 */
@Service
public class CommentService {

    private final CommentRepository commentRepository;

    @Autowired
    public CommentService(CommentRepository commentRepository) {
        this.commentRepository = commentRepository;
    }

    public CommentEntity findByIdAndAcceptedStatus(long id) {
        CommentEntity commentEntity = commentRepository.findByIdAndStatus(id, Status.ACCEPTED);
        if (commentEntity == null) {
            throw new NoSuchElementException();
        }

        return commentEntity;
    }

    public List<CommentEntity> findByAcceptedStatus() {
        return commentRepository.findByStatus(Status.ACCEPTED);
    }

    public boolean update(CommentUpdateRequest request) {
        CommentEntity commentEntity = commentRepository.findOne(request.getId());
        if (commentEntity == null) {
            throw new NoSuchElementException();
        }

        commentEntity.setContent(request.getContent());
        commentRepository.save(commentEntity);

        return true;
    }

    public CommentEntity save(CommentRequest request) {
        CommentEntity commentEntity = new CommentEntity();
        commentEntity.setContent(request.getContent());
        commentEntity.setStatus(Status.ACCEPTED);

        return commentRepository.save(commentEntity);
    }

    public boolean deleteAll() {
        commentRepository.deleteAll();

        return true;
    }

    public boolean delete(long id) {
        commentRepository.delete(id);

        return true;
    }
}
