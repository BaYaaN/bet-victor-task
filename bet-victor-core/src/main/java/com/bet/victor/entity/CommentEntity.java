package com.bet.victor.entity;

import com.bet.victor.domain.Status;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * Created by Jakub_Baran on 5/6/2016.
 */
@Entity
@Table(name = "comment")
@SequenceGenerator(name = "name.seq_comment", sequenceName = "seq_comment", allocationSize = 1, initialValue = 1)
@Getter
@Setter
public class CommentEntity {

    @Id
    @GeneratedValue(generator = "name.seq_comment", strategy = GenerationType.SEQUENCE)
    private Long id;

    @Column(name = "content")
    @NotNull
    //TODO: add max length
    private String content;

    @Column(name = "status")
    @Enumerated(EnumType.STRING)
    @NotNull
    Status status;
}
