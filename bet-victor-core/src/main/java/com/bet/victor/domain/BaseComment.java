package com.bet.victor.domain;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;

/**
 * Created by Kuba on 2016-05-08.
 */
@Getter
@Setter
public abstract class BaseComment {
    @NotNull
    private String content;
}
