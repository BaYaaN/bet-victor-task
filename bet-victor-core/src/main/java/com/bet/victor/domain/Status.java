package com.bet.victor.domain;

/**
 * Created by Kuba on 2016-05-06.
 */
public enum Status {
    ACCEPTED, REJECTED, TO_VERIFY;
}
