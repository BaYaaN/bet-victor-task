package com.bet.victor.domain;

import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by Kuba on 2016-05-08.
 */
@Getter
@Setter
public class CommentUpdateRequest extends CommentRequest {
    @NotNull
    private Long id;
}
