package com.bet.victor.domain;

/**
 * Created by Jakub_Baran on 11/3/2015.
 */

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by Kuba on 2016-05-08.
 */
@Getter
@Setter
@Builder
public class ExceptionData {

    private int httpStatus;
    private String message;

    @Override
    public String toString() {
        return new StringBuilder()
                .append("Http status: ").append(getHttpStatus()).append(", ")
                .append("message: ").append(getMessage()).append(", ").toString();
    }
}
