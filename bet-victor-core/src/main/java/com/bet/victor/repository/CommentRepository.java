package com.bet.victor.repository;

import com.bet.victor.domain.Status;
import com.bet.victor.entity.CommentEntity;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Created by Jakub_Baran on 5/6/2016.
 */
public interface CommentRepository extends CrudRepository<CommentEntity, Long> {

    List<CommentEntity> findByStatus(Status status);

    CommentEntity findByIdAndStatus(long id, Status status);
}
