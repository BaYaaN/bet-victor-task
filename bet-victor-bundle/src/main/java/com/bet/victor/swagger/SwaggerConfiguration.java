package com.bet.victor.swagger;

import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static com.google.common.base.Predicates.not;
import static com.google.common.base.Predicates.or;

/**
 * Created by Kuba on 2016-05-08.
 */
@Configuration
@EnableSwagger2
@ConfigurationProperties(prefix = "swagger")
public class SwaggerConfiguration {

    @Value("${management.contextPath}")
    private String contextPath;

    @Value("${swagger.inclusions}")
    private List<String> inclusions = new ArrayList<>();

    @Value("${swagger.exclusions}")
    private List<String> exclusions = new ArrayList<>();

    public List<String> getInclusions() {
        return inclusions;
    }

    public List<String> getExclusions() {
        return exclusions;
    }

    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2).useDefaultResponseMessages(false).apiInfo(apiInfo()).select()
                .paths(mergedInclusionsAndExclusions()).build();
    }

    private Predicate<String> mergedInclusionsAndExclusions() {
        Predicate<String> excludesPredicate = not(includeAllFromList(exclusions));
        Predicate<String> includesPredicate = includeAllFromList(inclusions);
        return Predicates.or(excludesPredicate, includesPredicate);
    }

    private Predicate includeAllFromList(List<String> predicates) {
        return or(arrayOfPredicates(predicates));
    }

    private Predicate[] arrayOfPredicates(List<String> paths) {
        Predicate[] predicates = paths.stream().map(PathSelectors::regex).collect(Collectors.toList())
                .toArray(new Predicate[paths.size()]);
        return predicates;
    }

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder().title("BetVictor").build();
    }
}
