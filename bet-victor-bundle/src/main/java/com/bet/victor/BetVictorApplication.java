package com.bet.victor;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.orm.jpa.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableJpaRepositories(basePackages = "com.bet.victor.repository")
@EntityScan(basePackages = "com.bet.victor.entity")
@ComponentScan({ "com.bet.victor" })
public class BetVictorApplication {

    public static void main(String[] args) {
        SpringApplication.run(BetVictorApplication.class, args);
    }
}
