CREATE TABLE comment (
    id bigserial primary key,
    content varchar(500) NOT NULL,
    status varchar(10) NOT NULL
);

create sequence seq_comment start with 1 increment by 1;
