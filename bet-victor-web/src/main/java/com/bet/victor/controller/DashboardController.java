package com.bet.victor.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

/**
 * Created by Kuba on 2016-05-08.
 */
@Controller
public class DashboardController {

    @RequestMapping(value = "/index", method = GET)
    public String index() {
        return "static/index.html";
    }

}
