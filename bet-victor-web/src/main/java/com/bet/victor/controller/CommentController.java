package com.bet.victor.controller;

import com.bet.victor.common.RequestMapper;
import com.bet.victor.domain.CommentRequest;
import com.bet.victor.domain.CommentUpdateRequest;
import com.bet.victor.entity.CommentEntity;
import com.bet.victor.service.CommentService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.ws.rs.core.MediaType;
import java.util.List;

import static org.springframework.http.HttpStatus.OK;
import static org.springframework.web.bind.annotation.RequestMethod.*;

/**
 * Created by Kuba on 2016-05-08.
 */
@RestController
@RequestMapping(value = RequestMapper.URL_COMMENT_V1)
@Api(value = "comment", description = "Common operations for comments")
public class CommentController {

    private final CommentService commentService;

    @Autowired
    public CommentController(CommentService commentService) {
        this.commentService = commentService;
    }

    /**
     * @param commentId
     * @return comment find by given id and ACCEPTED status
     */
    @RequestMapping(value = "/{comment-id}", method = GET, produces = MediaType.APPLICATION_JSON)
    @ApiOperation(value = "Get comment by ID and ACCEPTED status", response = CommentEntity.class)
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Successful retrieval of ACCEPTED comment")})
    public ResponseEntity<CommentEntity> findByIdAndAcceptedStatus(@PathVariable("comment-id") long commentId) {
        return new ResponseEntity<>(commentService.findByIdAndAcceptedStatus(commentId), OK);
    }


    /**
     * @return list of all comments with ACCEPTED status
     */
    @RequestMapping(method = GET, produces = MediaType.APPLICATION_JSON)
    @ApiOperation(value = "Get all comments with ACCEPTED status", response = CommentEntity.class)
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Successful retrieval of ACCEPTED comments")})
    public ResponseEntity<List<CommentEntity>> findByStatus() {
        return new ResponseEntity<>(commentService.findByAcceptedStatus(), OK);
    }

    /**
     * @param commentRequest
     * @return boolean value indicates if operation succeeded or failed
     */
    @RequestMapping(method = PUT, produces = MediaType.APPLICATION_JSON, consumes = MediaType.APPLICATION_JSON)
    @ApiOperation(value = "Update comment with given ID", response = Boolean.class)
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Successful update of comment")})
    public ResponseEntity<Boolean> update(@RequestBody @Valid CommentUpdateRequest commentRequest) {
        return new ResponseEntity<>(commentService.update(commentRequest), OK);
    }

    /**
     *
     * @param request with message content to be persist in database
     * @return comment with content
     */
    @MessageMapping("/save")
    @SendTo("/topic/save")
    public CommentEntity save(CommentRequest request) {
        return commentService.save(request);
    }

    /**
     * @param commentId
     * @return boolean value indicates if operation succeeded or failed
     */
    @RequestMapping(value = "/{comment-id}", method = DELETE, produces = MediaType.APPLICATION_JSON)
    @ApiOperation(value = "Delete comment with given ID", response = Boolean.class)
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Successful delete of comment")})
    public ResponseEntity<Boolean> delete(@PathVariable("comment-id") long commentId) {
        return new ResponseEntity<>(commentService.delete(commentId), OK);
    }

    /**
     * @return boolean value indicates if operation succeeded or failed
     */
    @RequestMapping(method = DELETE, produces = MediaType.APPLICATION_JSON)
    @ApiOperation(value = "Delete all comments", response = Boolean.class)
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Successful delete of comments")})
    public ResponseEntity<Boolean> deleteAll() {
        return new ResponseEntity<>(commentService.deleteAll(), OK);
    }
}
