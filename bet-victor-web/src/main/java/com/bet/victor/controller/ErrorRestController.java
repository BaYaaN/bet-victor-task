package com.bet.victor.controller;

import com.bet.victor.domain.ExceptionData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.NoSuchElementException;

/**
 * Created by Kuba on 2016-05-08.
 */
@ControllerAdvice(annotations = RestController.class)
public class ErrorRestController {
    private static final Logger LOGGER = LoggerFactory.getLogger(ErrorRestController.class);

    @ExceptionHandler(NoSuchElementException.class)
    @ResponseBody
    public ResponseEntity<ExceptionData> handleIllegalArgumentException(NoSuchElementException exception) {
        ExceptionData exceptionData = ExceptionData.builder()
                .httpStatus(HttpStatus.INTERNAL_SERVER_ERROR.value())
                .message("Can not find entity with given ID. Please provide correct ID !")
                .build();
        LOGGER.error(exception.toString(), exception);

        return new ResponseEntity<>(exceptionData, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(IllegalArgumentException.class)
    @ResponseBody
    public ResponseEntity<ExceptionData> handleIllegalArgumentException(IllegalArgumentException exception) {
        ExceptionData exceptionData = ExceptionData.builder()
                .httpStatus(HttpStatus.BAD_REQUEST.value())
                .message(" Method has been passed an illegal or inappropriate argument ")
                .build();
        LOGGER.error(exception.toString(), exception);

        return new ResponseEntity<>(exceptionData, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(RuntimeException.class)
    @ResponseBody
    public ResponseEntity<ExceptionData> handleRuntimeException(RuntimeException exception) {
        ExceptionData exceptionData = ExceptionData.builder()
                .httpStatus(HttpStatus.INTERNAL_SERVER_ERROR.value()).message("Upsss something went wrong")
                .build();
        LOGGER.error(exception.toString(), exception);

        return new ResponseEntity<>(exceptionData, HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
